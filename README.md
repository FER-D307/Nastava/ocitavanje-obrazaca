# Upute za očitavanje obrazaca (ZEMRIS)

Upute čini datoteka [`upute.md`](https://gitlab.com/FER-D307/Nastava/ocitavanje-obrazaca/-/blob/master/upute.md) s direktorijem `images`. 

Upute se još mogu naći u drugim formatima (HTML, PDF) [ovdje](https://gitlab.com/FER-D307/Nastava/ocitavanje-obrazaca/-/artifacts).

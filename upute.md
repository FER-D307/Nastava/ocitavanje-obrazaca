# 1. Skeniranje

1. Stvoriti direktorije za skenirane obrasce. Npr. za međuispit iz Digitalne logike oni mogu biti u `F:/dl/mi22`.
2. Uključiti skener.
4. Pokrenuti program FreeFormScanner <!-- pokretanjem `C:/usr/FreeFormSuite/FreeFormScanner2/run_x86.bat`  -->
5. Za "Base directory" odabrati prazan direktorij u čije poddirektorije program treba upisivati slike skeniranih obrazaca.
5. Za svaku dvoranu skenirati obrasce:
    1.  Pod "Room" unijeti ime dvorane. Glavni prozor onda izgleda slično sljedećoj slici.  
    [<img src="images/sken1.png" style="max-width: 250px; max_height: 250px" />](images/sken1.png)  
    2. Dok nisu skenirani svi obrasci:
        1.  U skener ubaciti prikladan broj obrazaca pa kliknuti `Scan / show GUI`. Onda se otvori prozor kao na sljedećoj slici. Postavke trebaju biti otprilike kao na slici. `Scanning Option: Scan Ahead` je za brže skeniranje. Kliknuti `Scan` i čekati dok ne završi skeniranje.  
        [<img src="images/sken2.png" style="max-width: 250px; max_height: 250px" />](images/sken2.png)   
        2. Nakon skeniranja glavni prozor izgleda kao na slici.  
        [<img src="images/sken3.png" style="max-width: 250px; max_height: 250px" />](images/sken3.png)  
6. Na kraju trebamo dobiti datotečnu strukturu kao na slici.  
[<img src="images/struktura.png" style="max-width: 250px; max_height: 250px" />](images/struktura.png)  
**Napomene:** 
    - Treba paziti da broj skeniranih obrazaca odgovara stvarnom broju obrazaca. Skener nekad povuče 2 papira zajedno (češće zadnja 2). 
    - Povlačenje zadnja 2 papira zajedo zbog skliske površine nekad se može sprječiti stavljanjem nekog papira ili neke druge manje skliske podloge ispod.


# 2. Očitavanje obrazaca

## 2.1. Stvaranje projekta

1. Pokrenuti program FreeFormReader.
2. Odabrati `File -> New Project` i na put direktorija ispita dodati ime nepostojećeg direktorija za projekt pa kliknuti `Save`.
Na primjeru na slici projektu dajemo ime "reader".  
[<img src="images/ffr-np1.png" style="max-width: 250px; max_height: 250px" />](images/ffr-np1.png)
[<img src="images/ffr-np2.png" style="max-width: 250px; max_height: 250px" />](images/ffr-np2.png)  
Glavni prozor onda treba izgledati kao na slici.  
[<img src="images/ffr-np3.png" style="max-width: 250px; max_height: 250px" />](images/ffr-np3.png)  
**Napomene:**
    - Postojeći projekt se može otvoriti preko `File -> Open Project` i odabira direktorija projekta.

## 2.2. Stvaranje predloška sadržaja obrazaca za očitavanje

1. Odabrati `File -> Add Template` i odabrati jednu od skeniranih slika.  
[<img src="images/ffr-temp1.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp1.png)
[<img src="images/ffr-temp2.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp2.png)
[<img src="images/ffr-temp3.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp3.png)  
Dobije se prozor kao na sljedećoj slici.  
[<img src="images/ffr-temp4.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp4.png)  
2. Prvo dodati ključ, koji je u ovom slučaju JMBAG zapisan barkodom:
    1. Kliknuti `Dodaj bar kod` i dodijeliti mu proizvoljno ime, npr. "barkod" ili "jmbag".  
    [<img src="images/ffr-temp-barkod1.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-barkod1.png)
    [<img src="images/ffr-temp-barkod2.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-barkod2.png)  
    2. Označiti barkod kao na slici klikom u gornji lijevi kut i donji desni kut pa kliknuti `OK`.  
    [<img src="images/ffr-temp-barkod3.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-barkod3.png)
    [<img src="images/ffr-temp-barkod4.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-barkod4.png)  
    3. Otvori se prozor postavki očitavanja barkoda. Postaviti sve kao na slici: unijeti "0" za sufiks i prefiks i označiti kućicu "Polje je ključ".  
    [<img src="images/ffr-temp-barkod5.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-barkod5.png)  
    Time se dodaje polje tipa `BAR CODE` u popis polja lijevo.  
3. Dodati polje grupe:
    1. Kliknuti `Dodaj grupu` i polju dodijeliti proizvoljno ime, npr. "grupa".  
    [<img src="images/ffr-temp-grupa1.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-grupa1.png)
    [<img src="images/ffr-temp-grupa2.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-grupa2.png)  
    2. Označiti polje grupe kao na slici pa kliknuti `OK`.  
    [<img src="images/ffr-temp-grupa3.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-grupa3.png)
    [<img src="images/ffr-temp-grupa4.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-grupa4.png)
    [<img src="images/ffr-temp-grupa5.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-grupa5.png)  
    3. Otvori se prozor za odabir od kojeg slova kreću grupe. Ostaviti odabir "A".  
    [<img src="images/ffr-temp-grupa6.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-grupa6.png)  
    Time se dodaje polje tipa `ABC RECT` u popis polja.
4. Dodati polje odgovora:  
    1. Kliknuti `Dodaj IrregularHorizontalABC`, polju dodijeliti proizvoljno ime (npr. "odgovori") pa kliknuti `OK`.  
    [<img src="images/ffr-temp-odgovori1.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-odgovori1.png)
    [<img src="images/ffr-temp-odgovori2.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-odgovori2.png)  
    2. Označiti polje odgovora kao na slici pa kliknuti `OK`.  
    [<img src="images/ffr-temp-odgovori3.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-odgovori3.png)
    [<img src="images/ffr-temp-odgovori4.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-odgovori4.png)  
    3. Otvori se prozor koji pita "Želite li blokirati koji dio slike". Kliknuti `No`.  
    [<img src="images/ffr-temp-odgovori5.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-odgovori4.png)  
    4. Otvori se prozor postavki. Označiti kućicu "Ima li zastavica pogreške'" i sve drugo ostaviti kao na slici. Kliknuti `OK`.  
    [<img src="images/ffr-temp-odgovori6.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-odgovori5.png)  
    Na kraju se pokaže prozor s označenim rasponatim kućicama na skeniranom obrascu, koji treba zatvoriti.  
    [<img src="images/ffr-temp-odgovori7.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-odgovori6.png)  
    Time se dodaje polje tipa `ABC IRR` u popis polja.
5. Označiti položaj lokatora:
    1. Otvoriti `File -> Select KS locator`, odabrati "m2" pa kliknuti `OK`.  
    [<img src="images/ffr-temp-loc1.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-loc1.png)
    [<img src="images/ffr-temp-loc2.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-loc2.png)
    2. Klikovima miša označiti vanjske kutove kvadrata (gore lijevo pa gore desno pa dolje lijevo, kao na slikama) pa kliknuti `OK`.  
    [<img src="images/ffr-temp-loc3.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-loc3.png)
    [<img src="images/ffr-temp-loc4.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-loc4.png)
    [<img src="images/ffr-temp-loc5.png" style="max-width: 250px; max_height: 250px" />](images/ffr-temp-loc5.png)  

## 2.3. Očitavanje obrazaca

1. Obraditi slike:
    1. Odabrati `Images -> Add image directories`, označiti direktorije koji sadrže skenirane obrasce pa kliknuti `OK`.  
    [<img src="images/ffr-images1.png" style="max-width: 250px; max_height: 250px" />](images/ffr-images1.png)
    [<img src="images/ffr-images2.png" style="max-width: 250px; max_height: 250px" />](images/ffr-images2.png)  
    2. Kliknuti na karticu `Images`, označiti sve slike klikom na jednu i pritiskom tipki `Ctrl+A` pa kliknuti na `Start selected`.  **Napomena: ako se koristi Java verzije 15 ili više, javljat će se greške zbog [nepostojanja implementacije JavaScripta](https://stackoverflow.com/questions/71481562/use-javascript-scripting-engine-in-java-17) i obrasci se neće ispravno očitati.**  
    [<img src="images/ffr-images3.png" style="max-width: 250px; max_height: 250px" />](images/ffr-images3.png)
    [<img src="images/ffr-images4.png" style="max-width: 250px; max_height: 250px" />](images/ffr-images4.png)  
    3. Kad program nešto ne može očitati, traži unos vrijednosti. 
    U prozoru koji se otvori, gore desno piše što treba unijeti, a na slici obrasca je označeno dokle je došlo očitavanje.
    Ako se vrijednost ne očita, unijeti ju i pritisnuti `Enter`.
    Ako je očitana greška u odgovoru, treba prepisati odgovor iz zadnje kućice ili ostaviti prazno ako nema odgovora.  
    [<img src="images/ffr-images6.png" style="max-width: 250px; max_height: 250px" />](images/ffr-images6.png)
    [<img src="images/ffr-images7.png" style="max-width: 250px; max_height: 250px" />](images/ffr-images7.png)  
    Ako JMBAG nije očitan, treba ga prepisati sa slike obrasca (bez dodavanje prefiksa i sufiksa).  
    [<img src="images/ffr-images-jmbag.png" style="max-width: 250px; max_height: 250px" />](images/ffr-images-jmbag.png)  
    4. Kad završi očitavanje, ako je nešto bilo krivo upisano, vrijednost nekog polja možemo promijeniti nakon dvoklika na redak slike obrasca.  
    [<img src="images/ffr-images8.png" style="max-width: 250px; max_height: 250px" />](images/ffr-images8.png)
    [<img src="images/ffr-images9.png" style="max-width: 250px; max_height: 250px" />](images/ffr-images9.png)  
2. Izvesti u tekstualnu datoteku koja tablično povezuje JMBAG s odgovorima i slikama obrazaca (`RMK`).
    1. Odabrati `Export -> Export podataka`.  
    [<img src="images/ffr-export1.png" style="max-width: 250px; max_height: 250px" />](images/ffr-export1.png)  
    2. Kliknuti `New from template`, unijeti "FakeRMK" pa kliknuti `OK`.  
    [<img src="images/ffr-export2.png" style="max-width: 250px; max_height: 250px" />](images/ffr-export2.png)
    [<img src="images/ffr-export3.png" style="max-width: 250px; max_height: 250px" />](images/ffr-export3.png)  
    3. Označiti kućicu "Fake RMK file" pa kliknuti `OK`.  
    [<img src="images/ffr-export4.png" style="max-width: 250px; max_height: 250px" />](images/ffr-export4.png)
    4. Otvori s prozor s poljima. Osigurati da je redoslijed polja "jmbag, grupa, odgovori" pa kliknuti `OK`.  
    [<img src="images/ffr-export5.png" style="max-width: 250px; max_height: 250px" />](images/ffr-export5.png)  
    5. Glavni prozor za izvoz onda izgleda kao na sljedećoj slici. Kliknuti `Export`, unijeti "fake.rmk" za ime datoteke pa kliknuti `Save`.  
    [<img src="images/ffr-export6.png" style="max-width: 250px; max_height: 250px" />](images/ffr-export6.png)
    [<img src="images/ffr-export7.png" style="max-width: 250px; max_height: 250px" />](images/ffr-export7.png)
    [<img src="images/ffr-export8.png" style="max-width: 250px; max_height: 250px" />](images/ffr-export8.png)  
    Datoteka "fake.rmk" izgleda kao na sljedećoj slici.  
    [<img src="images/ffr-export10.png" style="max-width: 250px; max_height: 250px" />](images/ffr-export10.png)  


## 2.4. Priprema podataka za objavljivanje

1. Odabrati `Tools -> Prepare RMK`.  
[<img src="images/ffr-prepare1.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare1.png)  
2. Otvori se prozor na sljedećoj slici. Kliknuti na `Browse` pod "Input RMK file", odabrati prethodno stvorenu datoteku "fake.rmk" pa kliknuti `Open`.  
[<img src="images/ffr-prepare2.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare2.png)
[<img src="images/ffr-prepare3.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare3.png)  
2. Kliknuti na `Browse` pod "output RMK file", u otvoreni prozor upisati ime nepostojeće datoteke, "ferko" pa kliknuti `Save`.  
[<img src="images/ffr-prepare4.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare4.png)
[<img src="images/ffr-prepare5.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare5.png)  
Nakon toga bi ispod "Output RMK file" trebao pisati put datoteke imena "ferko.rmk".
3. Kliknuti na `Browse` pod "Destination directory for images", stvoriti direktorij "img_small", odabrati ga pa kliknuti `Save`.  
[<img src="images/ffr-prepare6.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare6.png)
[<img src="images/ffr-prepare7.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare7.png)
[<img src="images/ffr-prepare8.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare8.png)  
4. Pod "Additional prefix to add to image names" unijeti proizvoljni prefiks. Primjer prefiksa na slici je "zi22-".  
[<img src="images/ffr-prepare9.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare9.png)  
5. Pod "Ferko support" kliknuti na `Generate image descriptor`, ostaviti odabran korijenski direktorij pa kliknuti `Save`.  
[<img src="images/ffr-prepare10.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare10.png)
[<img src="images/ffr-prepare11.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare11.png)  
Nakon toga bi trebala biti kvačica uz `Generate image descriptor` i ispod bi trebao pisati put do datoteke s imenom "opisnik.txt".
6. Ispod toga kliknuti na `Generate ZIP archive with images`, unijeti proizvoljno ime (npr. "upload.zip") pa kliknuti `Save`.  
[<img src="images/ffr-prepare12.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare12.png)
[<img src="images/ffr-prepare13.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare13.png)  
7. Kliknuti `Start`.  
[<img src="images/ffr-prepare14.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare14.png)
[<img src="images/ffr-prepare15.png" style="max-width: 250px; max_height: 250px" />](images/ffr-prepare15.png)  
Nakon obrade stvore se datoteke "ferko.rmk" i "upload.zip" u korijenskom direktoriju. "upload.zip" sadrži "opisnik.txt' i umanjene slike obrazaca.
7. Nakon toga kliknuti `Close`.  

# 3. Objavljivanje rezultata na Ferku

1. Otvoriti stranicu detalja provjere znanja:
    1. U pregledniko otići na stranicu predmeta pa kliknuti na "Provjere znanja i zastavice" pa u retku odgovarajuće provjere znanja kliknuti na "Details".  
    [<img src="images/ffr-ferko1.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko1.png)
    [<img src="images/ffr-ferko2.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko2.png)  
2. Poslati očitane odgovore:
    1. Na stranici detalja provjere znanja kliknuti na karticu "Rezultati studenata" pa na "Unesi rezultate".  
    [<img src="images/ffr-ferko3.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko3.png)
    [<img src="images/ffr-ferko4.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko4.png)  
    2. Pod "Unos iz datoteke" pa "Datoteka" kliknuti na `Choose file` i odabrati datoteku "ferko.rmk".  
    [<img src="images/ffr-ferko5.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko5.png)
    [<img src="images/ffr-ferko6.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko6.png)
    3. Pod "Vrsta datoteke" odabrati "RMK datoteka" pa kliknuti `Ažuriraj`.  
    [<img src="images/ffr-ferko7.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko7.png)
    [<img src="images/ffr-ferko8.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko8.png)      
3. Poslati arhivu sa slikama obrazaca:
    1. Na stranici detalja provjere znanja kliknuti na karticu "Datoteke" pa kliknuti `Choose file` i odabrati arhivu sa slikama ("upload.zip").  
    [<img src="images/ffr-ferko9.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko9.png)
    [<img src="images/ffr-ferko10.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko10.png)
    [<img src="images/ffr-ferko11.png" style="max-width: 250px; max_height: 250px" />](images/ffr-ferko11.png)  
    Ako sve radi kako bismo htjeli, nakon nekog vremena sve bi trebalo biti poslano.  
4. Poslati točne odgovore: TODO
